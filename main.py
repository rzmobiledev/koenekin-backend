from flask import Flask, request
from flask_restx import Resource, Api
from dotenv import load_dotenv
import os

load_dotenv()

# port settings
port = os.environ.get("BE_PORT")

app = Flask(__name__)
api = Api(app)

todos = {}


@api.route("/<string:todo_id>")
class TodoSimple(Resource):
    def get(self, todo_id):
        return {todo_id: todos[todo_id]}

    def put(self, todo_id):
        todos[todo_id] = request.form["data"]
        return {todo_id: todos[todo_id]}


if __name__ == "__main__":
    app.run(debug=True, port=port, host="0.0.0.0")
