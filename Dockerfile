FROM python:3.8.16-slim as base
LABEL maintainer="Safrizal"

ENV PYTHONUNBUFFERED 1

FROM base as builder
RUN mkdir /install
WORKDIR /install

COPY installer/requirements.txt .
COPY ./installer/scripts /scripts

RUN apt update && pip install --upgrade pip && pip install --prefix="/install" -r requirements.txt && \
    adduser \
    --disabled-password \
    --no-create-home \
    rizal && \
    chmod -R +x /scripts

USER rizal

FROM base
COPY --from=builder /install /usr/local
COPY --from=builder /scripts /usr/local/bin
COPY . /app


WORKDIR /app

EXPOSE 5000