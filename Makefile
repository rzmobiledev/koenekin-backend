.DEFAULT_GOAL=help

# Variables
MAKE				:= make --no-print-directory

create-requirements: ## exporting poetry installation to prod and dev requirements.txt
	@poetry export  --without-hashes -f requirements.txt --output installer/test_requirements.txt --with dev

.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'